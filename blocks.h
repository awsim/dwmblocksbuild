//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		                                                /*Update Interval*/	/*Update Signal*/
	{"  ",         "kernel",		                                                0,		        0},
	
	{" "  ,         "battery",                                                              1,                      0},
	
	{"  ",       "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	        1,		        0},
	
	{"    ",       "cpu-usage",                                                            1,	                0},

	{"   ",       "cputemp",                                                               1,                      0},

	{" ",           "volume",		                                                1,		       10},
	
	{"    ",       "clock",		                                                1,		        0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
